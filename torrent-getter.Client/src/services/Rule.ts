export interface RuleItem {
    contains: string[]
}

export interface Rule {
    id: string,
    name: string,
    directory: string,
    items: RuleItem[]
}
