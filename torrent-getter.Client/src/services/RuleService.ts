import { Injectable } from "@angular/core";
import { RuleStore } from "./RuleStore";
import { Rule, RuleItem } from "./Rule";
import { environment } from "../environment/environment";

@Injectable({
    providedIn: 'root'
})
export class RuleService {
    store: RuleStore;

    constructor(store: RuleStore) {
        this.store = store;
    }

    private toRule(data: any) {
        
        return {
            id: data.id,
            name: data.name,
            directory: data.directory,
            items: data.items.map((x: any): RuleItem => {
                return {
                    contains: x.contains
                }
            })
        }
    }

    async load() {
        const response = await fetch(environment.API + '/rules');
        const data = await response.json();
        
        if (data.error) {
            throw new Error(data.error);
        }

        try {
            const rules: Rule[] = data.map((x: any): Rule => this.toRule(x));
            this.store.rules.set(rules);
        } catch {
            throw new Error('Invalid server response');
        }
    }

    async add(rule: Rule) {
        const response = await fetch(
            environment.API + '/rules/add',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: rule.name,
                    directory: rule.directory,
                    items: rule.items.map(x => {
                        return {
                            contains: x.contains
                        }
                    })
                })
            }
        );
        const data = await response.json();

        if (data.error) {
            throw new Error(data.error);
        }

        this.store.rules.update(rules => {
            return [...rules, this.toRule(data)];
        });
    }

    async update(rule: Rule) {
        const response = await fetch(
            environment.API + '/rules/update',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: rule.id,
                    name: rule.name,
                    directory: rule.directory,
                    items: rule.items.map(x => {
                        return {
                            contains: x.contains
                        }
                    })
                })
            }
        );
        const data = await response.json();

        if (data.error) {
            throw new Error(data.error);
        }

        this.store.rules.update(rules => {
            return rules.map((x) => {
                if (x.id === rule.id) {
                    return this.toRule(data);
                }
    
                return x;
            });
        });
    }

    async delete(id: string) {
        const response = await fetch(
            environment.API + '/rules/delete',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: id
                })
            }
        );

        const dataStr = await response.text();

        if (dataStr) {
            const data = JSON.parse(dataStr);

            if (data.error) {
                throw new Error(data.error);
            }
        }

        this.store.rules.update(rules => {
            return rules.filter(x => x.id !== id);
        });
    }
}
