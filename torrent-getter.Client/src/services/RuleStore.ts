import { Injectable, WritableSignal, signal } from "@angular/core";
import { Rule } from "./Rule";

@Injectable({
    providedIn: 'root'
})
export class RuleStore {
    rules: WritableSignal<Rule[]> = signal([]);
}
