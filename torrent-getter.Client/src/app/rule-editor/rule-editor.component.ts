import { AfterViewChecked, AfterViewInit, Component, ElementRef, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { Rule } from '../../services/Rule';
import { Modal } from 'bootstrap';
import { RuleService } from '../../services/RuleService';

interface RuleQueryItem {
    id: number,
    value: string,
    valueError: string | undefined
}

interface RuleQueryEditor {
    id: number,
    items: RuleQueryItem[]
}

@Component({
    selector: 'app-rule-editor',
    standalone: true,
    imports: [],
    templateUrl: './rule-editor.component.html',
    styleUrl: './rule-editor.component.css'
})
export class RuleEditorComponent implements AfterViewInit, AfterViewChecked, OnDestroy {
    isLoading: boolean = false;
    name: string = '';
    nameError: string | undefined;
    directory: string = '';
    directoryError: string | undefined;
    queries: RuleQueryEditor[] = [];
    focusQueryItemInput: string | undefined;
    modal: Modal | undefined;
    rule: Rule | undefined;
    openCb: (() => void) | undefined;

    ruleService: RuleService;

    @ViewChildren('txtQueryItemInput') txtQueryItemInputs: QueryList<ElementRef<HTMLInputElement>> = new QueryList();

    constructor(ruleService: RuleService) {
        this.ruleService = ruleService;
    }

    ngAfterViewInit() {
        const elem = document.getElementById('rule-editor-component');

        if (elem === null) {
            throw new Error('Modal element not found');
        }

        this.modal = new Modal(
            elem,
            {
                backdrop: 'static'
            }
        );
    }
    
    ngAfterViewChecked(): void {
        if (this.focusQueryItemInput === undefined) {
            return;
        }

        this.txtQueryItemInputs
            .filter(x => x.nativeElement.classList.contains(this.focusQueryItemInput!))
            .slice(-1)[0]
            .nativeElement.focus();
        this.focusQueryItemInput = undefined;
    }

    ngOnDestroy() {
        this.modal?.dispose();
        this.modal = undefined;
    }

    open(rule: Rule | undefined): Promise<void> {
        this.rule = rule;

        if (rule !== undefined) {
            this.name = rule.name;
            this.directory = rule.directory;
            this.queries = rule.items.map((x, idx) => {
                return {
                    id: idx,
                    items: x.contains.map((query, queryIdx) => {
                        return {
                            id: queryIdx,
                            value: query,
                            valueError: undefined
                        }
                    })
                };
            });
        } else {
            this.queries = [
                {
                    id: 1,
                    items: [
                        {
                            id: 1,
                            value: '',
                            valueError: undefined
                        }
                    ]
                }
            ];
        }

        this.modal?.show();
        
        return new Promise((resolve, _) => {
            this.openCb = resolve;
        });
    }

    close() {
        this.modal?.hide();
        this.openCb?.();
        this.openCb = undefined;
        this.rule = undefined;

        this.name = '';
        this.nameError = undefined;
        this.directory = '';
        this.directoryError = undefined;
        this.queries = [];
    }

    setName(event: any) {
        this.name = event.target.value;
    }

    setDirectory(event: any) {
        this.directory = event.target.value;
    }

    addQuery() {
        this.queries = [
            ...this.queries,
            {
                id: new Date().getTime(),
                items: [
                    {
                        id: new Date().getTime() + 1,
                        value: '',
                        valueError: undefined
                    }
                ]
            }
        ];
    }

    deleteQuery(queryId: number) {
        this.queries = this.queries.filter(x => x.id !== queryId);
    }

    addQueryItem(queryId: number) {
        this.focusQueryItemInput = queryId.toString(); // Give focus to the new input later.
        this.queries = this.queries.map(x => {
            if (x.id !== queryId) {
                return x;
            }

            return {
                ...x,
                items: [
                    ...x.items,
                    {
                        id: new Date().getTime() + 1,
                        value: '',
                        valueError: undefined
                    }
                ]
            };
        });
    }

    setQueryItem(queryId: number, itemId: number, event: any) {
        this.queries = this.queries.map(x => {
            if (x.id !== queryId) {
                return x;
            }

            return {
                ...x,
                items: x.items.map(item => {
                    if (item.id !== itemId) {
                        return item;
                    }

                    return {
                        ...item,
                        value: event.target.value
                    }
                })
            };
        });
    }

    deleteQueryItem(queryId: number, itemId: number) {
        this.queries = this.queries.map(x => {
            if (x.id !== queryId) {
                return x;
            }

            return {
                ...x,
                items: x.items.filter(item =>
                    item.id !== itemId
                )
            };
        });

        this.queries = this.queries.filter(x => x.items.length >= 1);
    }

    async save() {
        // Validate.
        let hasErrors = false;

        this.nameError = !this.name ? 'Name is empty' : undefined;
        this.directoryError = !this.directory ? 'Directory is empty' : undefined;

        if (this.nameError ||
            this.directoryError
        ) {
            hasErrors = true;
        }

        if (this.queries.length == 0) {
            alert('Queries cannot be empty')
            return;
        }

        this.queries = this.queries.map(x => {
            return {
                ...x,
                items: x.items.map(xx => {
                    if (!xx.value) {
                        hasErrors = true;
                    }

                    return {
                        ...xx,
                        valueError: !xx.value ? 'Value cannot be empty' : undefined
                    }
                })
            };
        });

        if (hasErrors) {
            return;
        }

        // Save.
        const rule: Rule = {
            id: this.rule?.id ?? '',
            name: this.name,
            directory: this.directory,
            items: this.queries.map(x => {
                return {
                    contains: x.items.map(item => item.value)
                }
            })
        };
        this.isLoading = true;
        
        try {
            if (this.rule === undefined) {
                await this.ruleService.add(rule);
            } else {
                await this.ruleService.update(rule);
            }

            this.close();
        } catch (ex: any) {
            alert(ex.message);
        } finally {
            this.isLoading = false;
        }
    }
}
