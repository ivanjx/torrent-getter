import { AfterViewInit, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { RuleService } from '../services/RuleService';

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [CommonModule, RouterOutlet],
    templateUrl: './app.component.html',
    styleUrl: './app.component.css'
})
export class AppComponent implements AfterViewInit {
    isLoading: boolean = true;
    ruleService: RuleService;

    constructor(ruleService: RuleService) {
        this.ruleService = ruleService;
    }
    
    async ngAfterViewInit() {
        try {
            await this.ruleService.load();
            this.isLoading = false;
        } catch (ex: any) {
            alert(ex.message);
        }
    }
}
