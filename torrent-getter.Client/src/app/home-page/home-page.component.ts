import { Component, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { Rule } from '../../services/Rule';
import { RuleEditorComponent } from '../rule-editor/rule-editor.component';
import { RuleStore } from '../../services/RuleStore';
import { RuleService } from '../../services/RuleService';

@Pipe({
    name: 'ruleItems',
    standalone: true
})
export class RuleItemsPipe implements PipeTransform {
    transform(rule: Rule): string {
        let elipsis = '';

        if (rule.items[0].contains.length > 4 ||
            rule.items.length > 1
        ) {
            elipsis = '...';
        }

        return rule.items[0].contains.slice(0, 4).join(', ') + elipsis;
    }
}

@Component({
    selector: 'app-home-page',
    standalone: true,
    imports: [RuleEditorComponent, RuleItemsPipe],
    templateUrl: './home-page.component.html',
    styleUrl: './home-page.component.css',
})
export class HomePageComponent {
    isLoading: boolean = false;
    ruleStore: RuleStore;
    ruleService: RuleService;

    @ViewChild(RuleEditorComponent) ruleEditor: RuleEditorComponent | undefined;

    constructor(ruleStore: RuleStore, ruleService: RuleService) {
        this.ruleStore = ruleStore;
        this.ruleService = ruleService;
    }

    async openEditor(rule: Rule | undefined) {
        if (this.ruleEditor === undefined) {
            return;
        }

        await this.ruleEditor.open(rule);
    }

    async delete(rule: Rule) {
        this.isLoading = true;

        try {
            await this.ruleService.delete(rule.id);
        } catch (ex: any) {
            alert(ex.message);
        } finally {
            this.isLoading = false;
        }
    }
}
