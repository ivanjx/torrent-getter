using System;
using torrent_getter.Services;

namespace torrent_getter.DbTests.Services;

public class FakeConfigurationService : IConfigurationService
{
    public string BasePath { get; } = "";
    public string ListenEndpoint { get; } = "";
    public string DatabasePath => "InMemorySample;Mode=Memory;Cache=Shared";
    public string? SeqEndpoint { get; }
    public string? SeqApiKey { get; }
    public string AuthUser { get; } = "";
    public string AuthPassword { get; } = "";
    public string TransmissionApi { get; } = "";
    public string TransmissionApiUsername { get; } = "";
    public string TransmissionApiPassword { get; } = "";
}
