using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.DbTests.Services;

[Collection("db")]
public class ScheduleRepositoryTest
{
    DbConn m_conn;
    ScheduleRepository m_repository;

    public ScheduleRepositoryTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_repository = new ScheduleRepository(m_conn);
    }

    [Fact]
    public async Task GetLastRssItemTimeAsync_Test()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            // Setup.
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO schedule_static (
                    last_time
                ) VALUES (
                    '2023-10-02 10:00:00'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            DateTime result = await m_repository.GetLastRssItemTimeAsync();
            
            // Assert.
            Assert.Equal(
                DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime(),
                result);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM schedule_static";
            await command.ExecuteNonQueryAsync();
        }
    }
    
    [Fact]
    public async Task GetLastRssItemTimeAsync_Default()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        try
        {
            // Test.
            DateTime result = await m_repository.GetLastRssItemTimeAsync();
            
            // Assert.
            Assert.Equal(default, result);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM schedule_static";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task SetLastRssItemTimeAsync_Test()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            // Setup.
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO schedule_static (
                    last_time
                ) VALUES (
                    '2023-10-02 10:00:00'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.SetLastRssItemTimeAsync(
                DateTime.Parse("2023-10-02 11:00:30").ToLocalTime().ToUniversalTime());
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT last_time
                    FROM schedule_static";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("2023-10-02 11:00:30", reader.GetString(0));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM schedule_static";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task GetLastDownloadTimeAsync_Test()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            // Setup.
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO schedules (
                    rule_id, last_time
                ) VALUES (
                    'r1', '2023-10-02 10:00:00'
                );
                INSERT INTO schedules (
                    rule_id, last_time
                ) VALUES (
                    'r2', '2023-10-02 11:00:00'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            DateTime result1 = await m_repository.GetLastDownloadTimeAsync("r1");
            DateTime result2 = await m_repository.GetLastDownloadTimeAsync("r2");
            DateTime result3 = await m_repository.GetLastDownloadTimeAsync("r3");
            
            // Assert.
            Assert.Equal(
                DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime(),
                result1);
            Assert.Equal(
                DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime(),
                result2);
            Assert.Equal(default, result3);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM schedules";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task SetLastDownloadTimeAsync_Test()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();

        {
            // Setup.
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO schedules (
                    rule_id, last_time
                ) VALUES (
                    'r1', '2023-10-02 10:00:00'
                );
                INSERT INTO schedules (
                    rule_id, last_time
                ) VALUES (
                    'r2', '2023-10-02 11:00:00'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_repository.SetLastDownloadTimeAsync(
                "r1",
                DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime());
            
            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT rule_id, last_time
                    FROM schedules";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r2", reader.GetString(0));
                Assert.Equal(
                    "2023-10-02 11:00:00",
                    reader.GetString(1));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r1", reader.GetString(0));
                Assert.Equal(
                    "2023-10-02 12:00:00",
                    reader.GetString(1));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM schedules";
            await command.ExecuteNonQueryAsync();
        }
    }
}
