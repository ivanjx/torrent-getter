using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.DbTests.Services;

[Collection("db")]
public class RulesRepositoryTest
{
    DbConn m_conn;
    RulesRepository m_repository;

    public RulesRepositoryTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_repository = new RulesRepository(m_conn);
    }

    [Fact]
    public async Task GetAsync_Test()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        // Setup.
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO rules (
                    id, name, directory
                ) VALUES (
                    'r1', 'rule1', 'dir1'
                );
                INSERT INTO rules (
                    id, name, directory
                ) VALUES (
                    'r2', 'rule2', 'dir2'
                );
                
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r1', 0, 'c11'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r1', 0, 'c12'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r1', 0, 'c13'
                );
                
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r2', 0, 'c21'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r2', 1, 'c22'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r2', 1, 'c23'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }
        
        try
        {
            // Test.
            Rule[] result = await m_repository.GetAsync();
            
            // Assert.
            Rule[] rules =
            [
                new Rule(
                    "r1",
                    "rule1",
                    "dir1",
                    [
                        new RuleItem(["c11", "c12", "c13"])
                    ]),
                new Rule(
                    "r2",
                    "rule2",
                    "dir2",
                    [
                        new RuleItem(["c21"]),
                        new RuleItem(["c22", "c23"])
                    ])
            ];
            string resultjson = JsonSerializer.Serialize(result);
            string rulesjson = JsonSerializer.Serialize(rules);
            Assert.Equal(rulesjson, resultjson);
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM rule_items;
                DELETE FROM rules;";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task SaveAsync_Test()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        await m_conn.InitAsync();
        
        // Setup.
        {
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO rules (
                    id, name, directory
                ) VALUES (
                    'r1', 'rule 1', 'dir1'
                );
                INSERT INTO rules (
                    id, name, directory
                ) VALUES (
                    'r2', 'rule 2', 'dir2'
                );
                
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r1', 0, 'c11'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r1', 0, 'c12'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r1', 0, 'c13'
                );
                
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r2', 0, 'c21'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r2', 1, 'c22'
                );
                INSERT INTO rule_items (
                    rule_id, item_idx, contain
                ) VALUES (
                    'r2', 1, 'c23'
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }
        
        try
        {
            // Test.
            Rule[] rules =
            [
                new Rule(
                    "r1",
                    "rule1",
                    "dir1",
                    [
                        new RuleItem(["c11", "c12", "c13"])
                    ]),
                new Rule(
                    "r2",
                    "rule2",
                    "dir2",
                    [
                        new RuleItem(["c21", "c22"])
                    ]),
                new Rule(
                    "r3",
                    "rule3",
                    "dir3",
                    [
                        new RuleItem(["c31"]),
                        new RuleItem(["c32", "c33"])
                    ])
            ];
            await m_repository.SaveAsync(rules);

            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT id, name, directory
                    FROM rules";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r1", reader.GetString(0));
                Assert.Equal("rule1", reader.GetString(1));
                Assert.Equal("dir1", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r2", reader.GetString(0));
                Assert.Equal("rule2", reader.GetString(1));
                Assert.Equal("dir2", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r3", reader.GetString(0));
                Assert.Equal("rule3", reader.GetString(1));
                Assert.Equal("dir3", reader.GetString(2));

                Assert.False(await reader.ReadAsync());
            }
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT
                        rule_id, item_idx, contain
                    FROM rule_items";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r1", reader.GetString(0));
                Assert.Equal(0, reader.GetInt32(1));
                Assert.Equal("c11", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r1", reader.GetString(0));
                Assert.Equal(0, reader.GetInt32(1));
                Assert.Equal("c12", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r1", reader.GetString(0));
                Assert.Equal(0, reader.GetInt32(1));
                Assert.Equal("c13", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r2", reader.GetString(0));
                Assert.Equal(0, reader.GetInt32(1));
                Assert.Equal("c21", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r2", reader.GetString(0));
                Assert.Equal(0, reader.GetInt32(1));
                Assert.Equal("c22", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r3", reader.GetString(0));
                Assert.Equal(0, reader.GetInt32(1));
                Assert.Equal("c31", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r3", reader.GetString(0));
                Assert.Equal(1, reader.GetInt32(1));
                Assert.Equal("c32", reader.GetString(2));

                Assert.True(await reader.ReadAsync());
                Assert.Equal("r3", reader.GetString(0));
                Assert.Equal(1, reader.GetInt32(1));
                Assert.Equal("c33", reader.GetString(2));

                Assert.False(await reader.ReadAsync());
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM rule_items;
                DELETE FROM rules;";
            await command.ExecuteNonQueryAsync();
        }
    }
}
