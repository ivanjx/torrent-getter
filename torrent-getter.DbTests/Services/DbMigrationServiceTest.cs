using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.DbTests.Services;

[Collection("db")]
public class DbMigrationServiceTest
{
    DbConn m_conn;
    DbMigrationService m_service;

    public DbMigrationServiceTest()
    {
        m_conn = new DbConn(
            new FakeConfigurationService());
        m_service = new DbMigrationService(m_conn);
    }

    [Fact]
    public async Task MigrateAsync_V2()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        
        {
            // Setup.
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                CREATE TABLE IF NOT EXISTS `rules` (
                    `num`       INTEGER PRIMARY KEY AUTOINCREMENT,
                    `id`        TEXT NOT NULL UNIQUE,
                    `name`      TEXT NOT NULL UNIQUE,
                    `directory` TEXT NOT NULL,
                    `watchday`  TEXT NOT NULL,
                    `watchtime` TEXT NOT NULL,
                    `watchmin`  INTEGER NOT NULL
                );

                CREATE TABLE IF NOT EXISTS `rule_items` (
                    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                    `rule_id`   TEXT NOT NULL,
                    `item_idx`  INTEGER NOT NULL,
                    `contain`   TEXT NOT NULL,
                    FOREIGN KEY (`rule_id`) REFERENCES `rules`(`id`)
                );

                CREATE TABLE IF NOT EXISTS `schedules` (
                    `rule_id`   TEXT PRIMARY KEY,
                    `last_time` TEXT NOT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `schedule_static` (
                    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                    `last_time` TEXT NOT NULL
                );
                
                INSERT INTO rules (
                    id, name, directory,
                    watchday, watchtime, watchmin
                ) VALUES (
                    'r1', 'rule1', '/mnt/r1',
                    'monday', '01:00', 10
                );
                INSERT INTO rules (
                    id, name, directory,
                    watchday, watchtime, watchmin
                ) VALUES (
                    'r2', 'rule2', '/mnt/r2',
                    'tuesday', '02:00', 20
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_service.MigrateAsync(default);

            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT * FROM rules";
                using SqliteDataReader reader = await command.ExecuteReaderAsync();

                Assert.True(await reader.ReadAsync());
                Assert.Equal(4, reader.FieldCount);
                Assert.Equal("r1", reader.GetString(1));
                Assert.Equal("rule1", reader.GetString(2));
                Assert.Equal("/mnt/r1", reader.GetString(3));

                Assert.True(await reader.ReadAsync());
                Assert.Equal(4, reader.FieldCount);
                Assert.Equal("r2", reader.GetString(1));
                Assert.Equal("rule2", reader.GetString(2));
                Assert.Equal("/mnt/r2", reader.GetString(3));

                Assert.False(await reader.ReadAsync());
            }

            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT version FROM version";
                object? result = await command.ExecuteScalarAsync();
                
                Assert.Equal(2L, result);
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM rules";
            await command.ExecuteNonQueryAsync();
        }
    }

    [Fact]
    public async Task MigrateAsync_UpToDate()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        
        {
            // Setup.
            using SqliteTransaction transaction = conn.BeginTransaction();
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                CREATE TABLE IF NOT EXISTS `version` (
                    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                    `version`   INTEGER NOT NULL
                );

                CREATE TABLE IF NOT EXISTS `rules` (
                    `num`       INTEGER PRIMARY KEY AUTOINCREMENT,
                    `id`        TEXT NOT NULL UNIQUE,
                    `name`      TEXT NOT NULL UNIQUE,
                    `directory` TEXT NOT NULL
                );

                CREATE TABLE IF NOT EXISTS `rule_items` (
                    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                    `rule_id`   TEXT NOT NULL,
                    `item_idx`  INTEGER NOT NULL,
                    `contain`   TEXT NOT NULL,
                    FOREIGN KEY (`rule_id`) REFERENCES `rules`(`id`)
                );

                CREATE TABLE IF NOT EXISTS `schedules` (
                    `rule_id`   TEXT PRIMARY KEY,
                    `last_time` TEXT NOT NULL
                );
                
                CREATE TABLE IF NOT EXISTS `schedule_static` (
                    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                    `last_time` TEXT NOT NULL
                );
                
                INSERT INTO version (
                    version
                ) VALUES (
                    2
                );";
            await command.ExecuteNonQueryAsync();
            await transaction.CommitAsync();
        }

        try
        {
            // Test.
            await m_service.MigrateAsync(default);

            // Assert.
            {
                using SqliteCommand command = conn.CreateCommand();
                command.CommandText = @"
                    SELECT version FROM version";
                object? result = await command.ExecuteScalarAsync();
                
                Assert.Equal(2L, result);
            }
        }
        finally
        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM version";
            await command.ExecuteNonQueryAsync();
        }
    }
}
