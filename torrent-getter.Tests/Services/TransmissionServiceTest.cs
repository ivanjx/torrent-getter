using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using MockHttp;
using MockHttp.Json;
using Moq;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.Tests.Services;

public class TransmissionServiceTest
{
    Mock<IConfigurationService> m_configurationService;
    Mock<IRandomService> m_randomService;
    MockHttpHandler m_handler;
    TransmissionService m_service;

    public TransmissionServiceTest()
    {
        m_configurationService = new Mock<IConfigurationService>(MockBehavior.Strict);
        m_randomService = new Mock<IRandomService>(MockBehavior.Strict);
        m_handler = new MockHttpHandler();
        Mock<IHttpClientFactory> clientFactory = new Mock<IHttpClientFactory>(MockBehavior.Strict);
        clientFactory
            .Setup(x => x.CreateClient(It.IsAny<string>()))
            .Returns(new HttpClient(m_handler));
        m_service = new TransmissionService(
            m_configurationService.Object,
            m_randomService.Object,
            clientFactory.Object);
    }

    [Fact]
    public async Task DownloadAsync_Test()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.TransmissionApi)
            .Returns("https://transmission.com:9999/rpc");
        m_configurationService
            .SetupGet(x => x.TransmissionApiUsername)
            .Returns("username");
        m_configurationService
            .SetupGet(x => x.TransmissionApiPassword)
            .Returns("password");

        m_randomService
            .Setup(x => x.GenerateInt())
            .Returns(199);

        m_handler
            .When(x => x
                .Method(HttpMethod.Post)
                .RequestUri("https://transmission.com:9999/rpc")
                .Header("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "method", "torrent-add" },
                        { "tag", 199 },
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                { "start", true },
                                { "bandwidthPriority", 0 },
                                { "downloadDir", "torrentdownloaddir" },
                                { "paused", false },
                                { "download-dir", "torrentdownloaddir" },
                                { "filename", "torrentname" }
                            }
                        }
                    }))
            .Respond(x => x
                .StatusCode(409)
                .Body("error")
                .Header("X-Transmission-Session-Id", "csrf123"));
        
        m_handler
            .When(x => x
                .Method(HttpMethod.Post)
                .RequestUri("https://transmission.com:9999/rpc")
                .Header("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
                .Header("X-Transmission-Session-Id", "csrf123")
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "method", "torrent-add" },
                        { "tag", 199 },
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                { "start", true },
                                { "bandwidthPriority", 0 },
                                { "downloadDir", "torrentdownloaddir" },
                                { "paused", false },
                                { "download-dir", "torrentdownloaddir" },
                                { "filename", "torrentname" }
                            }
                        }
                    }))
            .Respond(x => x
                .StatusCode(200)
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "tag", 199 },
                        { "result", "success" },
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                {
                                    "torrent-added",
                                    new Dictionary<string, object>()
                                    {
                                        { "id", 123 },
                                        { "hashString", "torrenthashstring" },
                                        { "name", "torrentname" }
                                    }
                                }
                            }
                        },
                    }));
        
        // Test.
        Exception? exception = null;
        
        try
        {
            await m_service.DownloadAsync(
                "torrentname",
                "torrentdownloaddir",
                CancellationToken.None);
        }
        catch (Exception ex)
        {
            exception = ex;
        }
        
        // Assert.
        m_handler.VerifyAll();
        Assert.Null(exception);
    }

    [Fact]
    public async Task DownloadAsync_ValidateTag()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.TransmissionApi)
            .Returns("https://transmission.com:9999/rpc");
        m_configurationService
            .SetupGet(x => x.TransmissionApiUsername)
            .Returns("username");
        m_configurationService
            .SetupGet(x => x.TransmissionApiPassword)
            .Returns("password");

        m_randomService
            .Setup(x => x.GenerateInt())
            .Returns(199);

        m_handler
            .When(x => x
                .Method(HttpMethod.Post)
                .RequestUri("https://transmission.com:9999/rpc")
                .Header("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "method", "torrent-add" },
                        { "tag", 199 },
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                { "start", true },
                                { "bandwidthPriority", 0 },
                                { "downloadDir", "torrentdownloaddir" },
                                { "paused", false },
                                { "download-dir", "torrentdownloaddir" },
                                { "filename", "torrentname" }
                            }
                        }
                    }))
            .Respond(x => x
                .StatusCode(200)
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "tag", 200 },
                        { "result", "success" },
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                {
                                    "torrent-added",
                                    new Dictionary<string, object>()
                                    {
                                        { "id", 123 },
                                        { "hashString", "torrenthashstring" },
                                        { "name", "torrentname" }
                                    }
                                }
                            }
                        },
                    }));
        
        // Test.
        Exception? exception = null;
        
        try
        {
            await m_service.DownloadAsync(
                "torrentname",
                "torrentdownloaddir",
                CancellationToken.None);
        }
        catch (Exception ex)
        {
            exception = ex;
        }
        
        // Assert.
        m_handler.VerifyAll();
        Assert.NotNull(exception);
    }

    [Fact]
    public async Task DownloadAsync_ValidateResult()
    {
        // Setup.
        m_configurationService
            .SetupGet(x => x.TransmissionApi)
            .Returns("https://transmission.com:9999/rpc");
        m_configurationService
            .SetupGet(x => x.TransmissionApiUsername)
            .Returns("username");
        m_configurationService
            .SetupGet(x => x.TransmissionApiPassword)
            .Returns("password");

        m_randomService
            .Setup(x => x.GenerateInt())
            .Returns(199);

        m_handler
            .When(x => x
                .Method(HttpMethod.Post)
                .RequestUri("https://transmission.com:9999/rpc")
                .Header("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "method", "torrent-add" },
                        { "tag", 199 },
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                { "start", true },
                                { "bandwidthPriority", 0 },
                                { "downloadDir", "torrentdownloaddir" },
                                { "paused", false },
                                { "download-dir", "torrentdownloaddir" },
                                { "filename", "torrentname" }
                            }
                        }
                    }))
            .Respond(x => x
                .StatusCode(200)
                .JsonBody(
                    new Dictionary<string, object>()
                    {
                        { "tag", 199 },
                        { "result", "duplicate" }, // or something like that.
                        {
                            "arguments",
                            new Dictionary<string, object>()
                            {
                                {
                                    "torrent-added",
                                    new Dictionary<string, object>()
                                    {
                                        { "id", 123 },
                                        { "hashString", "torrenthashstring" },
                                        { "name", "torrentname" }
                                    }
                                }
                            }
                        },
                    }));
        
        // Test.
        Exception? exception = null;
        
        try
        {
            await m_service.DownloadAsync(
                "torrentname",
                "torrentdownloaddir",
                CancellationToken.None);
        }
        catch (Exception ex)
        {
            exception = ex;
        }
        
        // Assert.
        m_handler.VerifyAll();
        Assert.NotNull(exception);
    }
}
