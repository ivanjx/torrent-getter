using System;
using System.Threading.Tasks;
using Moq;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.Tests.Services;

public class RunnerServiceTest
{
    Mock<IRulesRepository> m_rulesRepository;
    Mock<IScheduleRepository> m_scheduleRepository;
    Mock<INyaaRssService> m_nyaaRssService;
    Mock<ITransmissionService> m_transmissionService;
    RunnerService m_service;

    public RunnerServiceTest()
    {
        m_rulesRepository = new Mock<IRulesRepository>(MockBehavior.Strict);
        m_scheduleRepository = new Mock<IScheduleRepository>(MockBehavior.Strict);
        m_nyaaRssService = new Mock<INyaaRssService>(MockBehavior.Strict);
        m_transmissionService = new Mock<ITransmissionService>(MockBehavior.Strict);
        m_service = new RunnerService(
            m_rulesRepository.Object,
            m_scheduleRepository.Object,
            m_nyaaRssService.Object,
            m_transmissionService.Object);
    }

    [Fact]
    public async Task RunAsync_Test()
    {
        // Setup.
        m_scheduleRepository
            .Setup(x => x.GetLastRssItemTimeAsync())
            .ReturnsAsync(DateTime.MinValue);

        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(new[]
            {
                new Rule(
                    "r1",
                    "Firefighter Daigo",
                    "/share/Movies/Firefighter Daigo",
                    [
                        new RuleItem(["[ASW]", "Megumi", "Daigo", "Orange"])
                    ]),
                new Rule(
                    "r2",
                    "Dekoboko Majo",
                    "/share/Movies/Dekoboko Majo",
                    [
                        new RuleItem(["[ASW]", "Family", "Circumstances", "Witch"]),
                        new RuleItem(["[ASW]", "Dekoboko", "Majo", "Oyako", "Jijou"])
                    ])
            });
        
        m_nyaaRssService
            .Setup(x => x.GetItemsAsync(default))
            .ReturnsAsync(new[]
            {
                new RssItem(
                    "t1",
                    "[ASW] Megumi no Daigo - Kyuukoku no Orange - 01 [1080p HEVC][3B1F18F7].mkv",
                    "link1",
                    DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t2",
                    "[ASW] Dekoboko Majo no Oyako Jijou - 01 [1080p HEVC][8EE50DC9].mkv",
                    "link2",
                    DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t3",
                    "[ASW] Girls' Frontline - 01v2 [1080p HEVC][35C1A8A3].mkv",
                    "link3",
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime())
            });

        m_scheduleRepository
            .Setup(x => x.GetLastDownloadTimeAsync(It.IsAny<string>()))
            .ReturnsAsync(DateTime.MinValue);

        int downloadCount = 0;
        m_transmissionService
            .Setup(x =>
                x.DownloadAsync(
                    "link1",
                    "/share/Movies/Firefighter Daigo",
                    default))
            .Callback(() => ++downloadCount)
            .Returns(Task.CompletedTask);
        m_transmissionService
            .Setup(x =>
                x.DownloadAsync(
                    "link2",
                    "/share/Movies/Dekoboko Majo",
                    default))
            .Callback(() => ++downloadCount)
            .Returns(Task.CompletedTask);

        m_scheduleRepository
            .Setup(x =>
                x.SetLastDownloadTimeAsync(
                    "r1",
                    DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime()))
            .Callback(() => Assert.Equal(1, downloadCount))
            .Returns(Task.CompletedTask);
        m_scheduleRepository
            .Setup(x =>
                x.SetLastDownloadTimeAsync(
                    "r2",
                    DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime()))
            .Callback(() => Assert.Equal(2, downloadCount))
            .Returns(Task.CompletedTask);

        int setLastCount = 0;
        m_scheduleRepository
            .Setup(x =>
                x.SetLastRssItemTimeAsync(
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime()))
            .Callback(() => ++setLastCount)
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.RunAsync(default);
        
        // Assert.
        Assert.Equal(2, downloadCount);
        Assert.Equal(1, setLastCount);
    }

    [Fact]
    public async Task RunAsync_SkipOldRssItem()
    {
        // Setup.
        m_scheduleRepository
            .Setup(x => x.GetLastRssItemTimeAsync())
            .ReturnsAsync(DateTime.Parse("2023-10-02 11:00:00"));

        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(new[]
            {
                new Rule(
                    "r1",
                    "Firefighter Daigo",
                    "/share/Movies/Firefighter Daigo",
                    [
                        new RuleItem(["[ASW]", "Megumi", "Daigo", "Orange"])
                    ]),
                new Rule(
                    "r2",
                    "Dekoboko Majo",
                    "/share/Movies/Dekoboko Majo",
                    [
                        new RuleItem(["[ASW]", "Family", "Circumstances", "Witch"]),
                        new RuleItem(["[ASW]", "Dekoboko", "Majo", "Oyako", "Jijou"])
                    ])
            });
        
        m_nyaaRssService
            .Setup(x => x.GetItemsAsync(default))
            .ReturnsAsync(new[]
            {
                new RssItem(
                    "t1",
                    "[ASW] Megumi no Daigo - Kyuukoku no Orange - 01 [1080p HEVC][3B1F18F7].mkv",
                    "link1",
                    DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t2",
                    "[ASW] Dekoboko Majo no Oyako Jijou - 01 [1080p HEVC][8EE50DC9].mkv",
                    "link2",
                    DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t3",
                    "[ASW] Girls' Frontline - 01v2 [1080p HEVC][35C1A8A3].mkv",
                    "link3",
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime())
            });

        m_scheduleRepository
            .Setup(x => x.GetLastDownloadTimeAsync(It.IsAny<string>()))
            .ReturnsAsync(DateTime.MinValue);

        int setLastCount = 0;
        m_scheduleRepository
            .Setup(x =>
                x.SetLastRssItemTimeAsync(
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime()))
            .Callback(() => ++setLastCount)
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.RunAsync(default);
        
        // Assert.
        Assert.Equal(1, setLastCount);
    }

    [Fact]
    public async Task RunAsync_SkipOldRssItem2()
    {
        // Setup.
        m_scheduleRepository
            .Setup(x => x.GetLastRssItemTimeAsync())
            .ReturnsAsync(DateTime.Parse("2023-10-02 12:00:00"));

        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(new[]
            {
                new Rule(
                    "r1",
                    "Firefighter Daigo",
                    "/share/Movies/Firefighter Daigo",
                    [
                        new RuleItem(["[ASW]", "Megumi", "Daigo", "Orange"])
                    ]),
                new Rule(
                    "r2",
                    "Dekoboko Majo",
                    "/share/Movies/Dekoboko Majo",
                    [
                        new RuleItem(["[ASW]", "Family", "Circumstances", "Witch"]),
                        new RuleItem(["[ASW]", "Dekoboko", "Majo", "Oyako", "Jijou"])
                    ])
            });
        
        m_nyaaRssService
            .Setup(x => x.GetItemsAsync(default))
            .ReturnsAsync(new[]
            {
                new RssItem(
                    "t1",
                    "[ASW] Megumi no Daigo - Kyuukoku no Orange - 01 [1080p HEVC][3B1F18F7].mkv",
                    "link1",
                    DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t2",
                    "[ASW] Dekoboko Majo no Oyako Jijou - 01 [1080p HEVC][8EE50DC9].mkv",
                    "link2",
                    DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t3",
                    "[ASW] Girls' Frontline - 01v2 [1080p HEVC][35C1A8A3].mkv",
                    "link3",
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime())
            });
        
        // Test.
        await m_service.RunAsync(default);
    }

    [Fact]
    public async Task RunAsync_SkipNewDownload()
    {
        // Setup.
        m_scheduleRepository
            .Setup(x => x.GetLastRssItemTimeAsync())
            .ReturnsAsync(DateTime.MinValue);

        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(new[]
            {
                new Rule(
                    "r1",
                    "Firefighter Daigo",
                    "/share/Movies/Firefighter Daigo",
                    [
                        new RuleItem(["[ASW]", "Megumi", "Daigo", "Orange"])
                    ]),
                new Rule(
                    "r2",
                    "Dekoboko Majo",
                    "/share/Movies/Dekoboko Majo",
                    [
                        new RuleItem(["[ASW]", "Family", "Circumstances", "Witch"]),
                        new RuleItem(["[ASW]", "Dekoboko", "Majo", "Oyako", "Jijou"])
                    ])
            });
        
        m_nyaaRssService
            .Setup(x => x.GetItemsAsync(default))
            .ReturnsAsync(new[]
            {
                new RssItem(
                    "t1",
                    "[ASW] Megumi no Daigo - Kyuukoku no Orange - 01 [1080p HEVC][3B1F18F7].mkv",
                    "link1",
                    DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t2",
                    "[ASW] Dekoboko Majo no Oyako Jijou - 01 [1080p HEVC][8EE50DC9].mkv",
                    "link2",
                    DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime()),
                new RssItem(
                    "t3",
                    "[ASW] Girls' Frontline - 01v2 [1080p HEVC][35C1A8A3].mkv",
                    "link3",
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime())
            });

        m_scheduleRepository
            .Setup(x => x.GetLastDownloadTimeAsync("r1"))
            .ReturnsAsync(DateTime.MinValue);
        m_scheduleRepository
            .Setup(x => x.GetLastDownloadTimeAsync("r2"))
            .ReturnsAsync(DateTime.Parse("2023-10-02 11:00:00").ToLocalTime().ToUniversalTime());

        int downloadCount = 0;
        m_transmissionService
            .Setup(x =>
                x.DownloadAsync(
                    "link1",
                    "/share/Movies/Firefighter Daigo",
                    default))
            .Callback(() => ++downloadCount)
            .Returns(Task.CompletedTask);

        m_scheduleRepository
            .Setup(x =>
                x.SetLastDownloadTimeAsync(
                    "r1",
                    DateTime.Parse("2023-10-02 10:00:00").ToLocalTime().ToUniversalTime()))
            .Callback(() => Assert.Equal(1, downloadCount))
            .Returns(Task.CompletedTask);

        int setLastCount = 0;
        m_scheduleRepository
            .Setup(x =>
                x.SetLastRssItemTimeAsync(
                    DateTime.Parse("2023-10-02 12:00:00").ToLocalTime().ToUniversalTime()))
            .Callback(() => ++setLastCount)
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.RunAsync(default);
        
        // Assert.
        Assert.Equal(1, downloadCount);
        Assert.Equal(1, setLastCount);
    }
}
