using System;
using System.Text.Json;
using System.Threading.Tasks;
using Moq;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.Tests.Services;

public class RuleServiceTest
{
    Mock<IRulesRepository> m_rulesRepository;
    Mock<IRandomService> m_randomService;
    RuleService m_service;

    public RuleServiceTest()
    {
        m_rulesRepository = new Mock<IRulesRepository>(MockBehavior.Strict);
        m_randomService = new Mock<IRandomService>(MockBehavior.Strict);
        m_service = new RuleService(
            m_rulesRepository.Object,
            m_randomService.Object);
    }

    [Fact]
    public async Task AddAsync_Test()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);

        m_randomService
            .Setup(x => x.GenerateId())
            .Returns("3");
        
        // Test.
        AddRuleResult result = await m_service.AddAsync(
            "rule3",
            "dir3",
            [
                ["r6", "r5"]
            ]);
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r6", "r5"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);

        SuccessAddRuleResult success =
            Assert.IsType<SuccessAddRuleResult>(result);
        rulesjson = JsonSerializer.Serialize(success.Rule);
        repoRulesjson = JsonSerializer.Serialize(repoRules[2]);
        Assert.Equal(rulesjson, repoRulesjson);
    }

    [Fact]
    public async Task AddAsync_Duplicate()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);

        m_randomService
            .Setup(x => x.GenerateId())
            .Returns("3");
        
        // Test.
        AddRuleResult result = await m_service.AddAsync(
            "rule3",
            "dir3",
            [
                ["r2", "r1"]
            ]);
        
        // Assert.
        Assert.IsType<DuplicateAddRuleResult>(result);
    }

    [Fact]
    public async Task AddAsync_Duplicate2()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);

        m_randomService
            .Setup(x => x.GenerateId())
            .Returns("3");
        
        // Test.
        AddRuleResult result = await m_service.AddAsync(
            "rule3",
            "dir3",
            [
                ["r6"],
                ["r2", "r1"]
            ]);
        
        // Assert.
        Assert.IsType<DuplicateAddRuleResult>(result);
    }

    [Fact]
    public async Task UpdateAsync_Test()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule 3",
            "dir33",
            [
                ["r66", "r55"]
            ]);
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule 3",
                "dir33",
                [
                    new RuleItem(["r66", "r55"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);

        SuccessUpdateRuleResult success =
            Assert.IsType<SuccessUpdateRuleResult>(result);
        rulesjson = JsonSerializer.Serialize(success.Rule);
        repoRulesjson = JsonSerializer.Serialize(repoRules[1]);
        Assert.Equal(rulesjson, repoRulesjson);
    }

    [Fact]
    public async Task UpdateAsync_Test2()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule3",
            "dir3",
            [
                ["r55", "r66"] // Only change items.
            ]);
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),            
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r55", "r66"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);

        SuccessUpdateRuleResult success =
            Assert.IsType<SuccessUpdateRuleResult>(result);
        rulesjson = JsonSerializer.Serialize(success.Rule);
        repoRulesjson = JsonSerializer.Serialize(repoRules[1]);
        Assert.Equal(rulesjson, repoRulesjson);
    }

    [Fact]
    public async Task UpdateAsync_Test3()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"]),
                    new RuleItem(["r7", "r8"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule3",
            "dir3",
            [
                ["r5", "r6"] // Remove one item.
            ]);
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),            
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);

        SuccessUpdateRuleResult success =
            Assert.IsType<SuccessUpdateRuleResult>(result);
        rulesjson = JsonSerializer.Serialize(success.Rule);
        repoRulesjson = JsonSerializer.Serialize(repoRules[1]);
        Assert.Equal(rulesjson, repoRulesjson);
    }

    [Fact]
    public async Task UpdateAsync_Test4()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule3",
            "dir3",
            [
                ["r5", "r6"],
                ["r7", "r8"] // Add one item.
            ]);
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),            
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"]),
                    new RuleItem(["r7", "r8"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);

        SuccessUpdateRuleResult success =
            Assert.IsType<SuccessUpdateRuleResult>(result);
        rulesjson = JsonSerializer.Serialize(success.Rule);
        repoRulesjson = JsonSerializer.Serialize(repoRules[1]);
        Assert.Equal(rulesjson, repoRulesjson);
    }

    [Fact]
    public async Task UpdateAsync_Test5()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule3",
            "dir3",
            [
                ["r6", "r5"] // Change item order.
            ]);
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),            
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r6", "r5"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);

        SuccessUpdateRuleResult success =
            Assert.IsType<SuccessUpdateRuleResult>(result);
        rulesjson = JsonSerializer.Serialize(success.Rule);
        repoRulesjson = JsonSerializer.Serialize(repoRules[1]);
        Assert.Equal(rulesjson, repoRulesjson);
    }

    [Fact]
    public async Task UpdateAsync_Duplicate()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule2", // Name already used.
            "dir3",
            [
                ["r5", "r6"]
            ]);
        
        // Assert.
        Assert.IsType<DuplicateUpdateRuleResult>(result);
    }

    [Fact]
    public async Task UpdateAsync_Duplicate2()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        UpdateRuleResult result = await m_service.UpdateAsync(
            "3",
            "rule3",
            "dir3",
            [
                ["r5", "r6"],
                ["r4", "r3"] // Already owned by rule2.
            ]);
        
        // Assert.
        Assert.IsType<DuplicateUpdateRuleResult>(result);
    }

    [Fact]
    public async Task DeleteAsync_Test()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);

        m_rulesRepository
            .Setup(x => x.SaveAsync(It.IsAny<Rule[]>()))
            .Callback((Rule[] x) => repoRules = x)
            .Returns(Task.CompletedTask);
        
        // Test.
        await m_service.DeleteAsync("3");
        
        // Assert.
        Rule[] rules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        string rulesjson = JsonSerializer.Serialize(rules);
        string repoRulesjson = JsonSerializer.Serialize(repoRules);
        Assert.Equal(rulesjson, repoRulesjson);
    }
    
    [Fact]
    public async Task DeleteAsync_NonExistent()
    {
        // Setup.
        Rule[] repoRules =
        [
            new Rule(
                "1",
                "rule1",
                "dir1",
                [
                    new RuleItem(["r1", "r2"])
                ]),
            new Rule(
                "3",
                "rule3",
                "dir3",
                [
                    new RuleItem(["r5", "r6"])
                ]),
            new Rule(
                "2",
                "rule2",
                "dir2",
                [
                    new RuleItem(["r3", "r4"])
                ])
        ];
        m_rulesRepository
            .Setup(x => x.GetAsync())
            .ReturnsAsync(repoRules);
        
        // Test.
        await m_service.DeleteAsync("5");
    }
}
