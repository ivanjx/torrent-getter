using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using MockHttp;
using Moq;
using torrent_getter.Services;
using Xunit;

namespace torrent_getter.Tests.Services;

public class NyaaRssServiceTest
{
    MockHttpHandler m_handler;
    NyaaRssService m_service;

    public NyaaRssServiceTest()
    {
        m_handler = new MockHttpHandler();
        Mock<IHttpClientFactory> clientFactory = new Mock<IHttpClientFactory>(MockBehavior.Strict);
        clientFactory
	        .Setup(x => x.CreateClient(It.IsAny<string>()))
	        .Returns(new HttpClient(m_handler));
        m_service = new NyaaRssService(
            clientFactory.Object);
    }

    [Fact]
    public async Task GetItemsAsync_Test()
    {
        // Setup.
        m_handler
            .When(x => x
                .Method(HttpMethod.Get)
                .RequestUri("https://nyaa.si/rss"))
            .Respond(x => x
	            .StatusCode(200)
	            .Body(RSS)
	            .ContentType("application/xml"));
        
        // Test.
        RssItem[] result = await m_service.GetItemsAsync(CancellationToken.None);
        
        // Assert.
        Assert.Equal(3, result.Length);
        
        RssItem item1 = new RssItem(
	        "1715360",
	        "[Fushigi na G] Kamen Rider Gotchard - 02 (Script y fuentes) (Español neutro)",
	        "https://nyaa.si/download/1715360.torrent",
	        DateTime.Parse("Sun, 10 Sep 2023 07:29:12 -0000").ToUniversalTime());
        Assert.Equal(item1, result[0]);
        
        RssItem item2 = new RssItem(
	        "1715361",
	        "[Fushigi na G] Kamen Rider Gotchard - 01 (1080p WEB-DL) (Español neutro)",
	        "https://nyaa.si/download/1715361.torrent",
	        DateTime.Parse("Sun, 10 Sep 2023 07:29:16 -0000").ToUniversalTime());
        Assert.Equal(item2, result[1]);
        
        RssItem item3 = new RssItem(
	        "1715362",
	        "[EiGo] Kamen Rider Gotchard - 02 [7349FBF2].mkv",
	        "https://nyaa.si/download/1715362.torrent",
	        DateTime.Parse("Sun, 10 Sep 2023 07:38:20 -0000").ToUniversalTime());
        Assert.Equal(item3, result[2]);
    }

    const string RSS = @"<rss xmlns:atom=""http://www.w3.org/2005/Atom"" xmlns:nyaa=""https://nyaa.si/xmlns/nyaa"" version=""2.0"">
	<channel>
		<title>Nyaa - Home - Torrent File RSS</title>
		<description>RSS Feed for Home</description>
		<link>https://nyaa.si/</link>
		<atom:link href=""https://nyaa.si/?page=rss"" rel=""self"" type=""application/rss+xml"" />
		<item>
			<title>[EiGo] Kamen Rider Gotchard - 02 [7349FBF2].mkv</title>
				<link>https://nyaa.si/download/1715362.torrent</link>
				<guid isPermaLink=""true"">https://nyaa.si/view/1715362</guid>
				<pubDate>Sun, 10 Sep 2023 07:38:20 -0000</pubDate>

				<nyaa:seeders>0</nyaa:seeders>
				<nyaa:leechers>16</nyaa:leechers>
				<nyaa:downloads>0</nyaa:downloads>
				<nyaa:infoHash>c902e4cee0d4d6f4b311c5800885453b4c213fcc</nyaa:infoHash>
			<nyaa:categoryId>4_1</nyaa:categoryId>
			<nyaa:category>Live Action - English-translated</nyaa:category>
			<nyaa:size>621.7 MiB</nyaa:size>
			<nyaa:comments>0</nyaa:comments>
			<nyaa:trusted>No</nyaa:trusted>
			<nyaa:remake>No</nyaa:remake>
			<description><![CDATA[<a href=""https://nyaa.si/view/1715362"">#1715362 | [EiGo] Kamen Rider Gotchard - 02 [7349FBF2].mkv</a> | 621.7 MiB | Live Action - English-translated | C902E4CEE0D4D6F4B311C5800885453B4C213FCC]]></description>
		</item>
		<item>
			<title>[Fushigi na G] Kamen Rider Gotchard - 01 (1080p WEB-DL) (Español neutro)</title>
				<link>https://nyaa.si/download/1715361.torrent</link>
				<guid isPermaLink=""true"">https://nyaa.si/view/1715361</guid>
				<pubDate>Sun, 10 Sep 2023 07:29:16 -0000</pubDate>

				<nyaa:seeders>1</nyaa:seeders>
				<nyaa:leechers>0</nyaa:leechers>
				<nyaa:downloads>0</nyaa:downloads>
				<nyaa:infoHash>5d614892e9278627e80f9da807587b6e6a7b4ebf</nyaa:infoHash>
			<nyaa:categoryId>4_3</nyaa:categoryId>
			<nyaa:category>Live Action - Non-English-translated</nyaa:category>
			<nyaa:size>499.4 MiB</nyaa:size>
			<nyaa:comments>0</nyaa:comments>
			<nyaa:trusted>No</nyaa:trusted>
			<nyaa:remake>No</nyaa:remake>
			<description><![CDATA[<a href=""https://nyaa.si/view/1715361"">#1715361 | [Fushigi na G] Kamen Rider Gotchard - 01 (1080p WEB-DL) (Español neutro)</a> | 499.4 MiB | Live Action - Non-English-translated | 5D614892E9278627E80F9DA807587B6E6A7B4EBF]]></description>
		</item>
		<item>
			<title>[Fushigi na G] Kamen Rider Gotchard - 02 (Script y fuentes) (Español neutro)</title>
				<link>https://nyaa.si/download/1715360.torrent</link>
				<guid isPermaLink=""true"">https://nyaa.si/view/1715360</guid>
				<pubDate>Sun, 10 Sep 2023 07:29:12 -0000</pubDate>

				<nyaa:seeders>1</nyaa:seeders>
				<nyaa:leechers>0</nyaa:leechers>
				<nyaa:downloads>0</nyaa:downloads>
				<nyaa:infoHash>12305f0d6f28b454056378a7eeae851fe06d16bf</nyaa:infoHash>
			<nyaa:categoryId>4_3</nyaa:categoryId>
			<nyaa:category>Live Action - Non-English-translated</nyaa:category>
			<nyaa:size>1.9 MiB</nyaa:size>
			<nyaa:comments>0</nyaa:comments>
			<nyaa:trusted>No</nyaa:trusted>
			<nyaa:remake>No</nyaa:remake>
			<description><![CDATA[<a href=""https://nyaa.si/view/1715360"">#1715360 | [Fushigi na G] Kamen Rider Gotchard - 02 (Script y fuentes) (Español neutro)</a> | 1.9 MiB | Live Action - Non-English-translated | 12305F0D6F28B454056378A7EEAE851FE06D16BF]]></description>
		</item>
	</channel>
</rss>";
}
