using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Formatting.Compact;
using Serilog.Events;
using torrent_getter.Controllers;
using torrent_getter.Jobs;
using torrent_getter.Services;

ConfigurationService configurationService = new();

// Serilog.
var loggerConfiguration = new LoggerConfiguration()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    .MinimumLevel.Override("System", LogEventLevel.Warning)
    .Enrich.WithClientIp()
    .Enrich.WithCorrelationId()
    .Enrich.FromLogContext()
#if DEBUG
    .WriteTo.Debug(new RenderedCompactJsonFormatter());
#else
    .WriteTo.Console(new RenderedCompactJsonFormatter());
#endif

Log.Logger = loggerConfiguration.CreateLogger();

try
{
    WebApplicationBuilder builder = WebApplication.CreateSlimBuilder(args);
    builder.WebHost.UseUrls(configurationService.ListenEndpoint);
    builder.Host.UseSerilog();

    builder.Services
        // Controllers json serializer context.
        .ConfigureHttpJsonOptions(options =>
        {
            options.SerializerOptions.TypeInfoResolverChain.Insert(
                0,
                ControllerJsonContext.Default);
        })

        // Forwarder.
        .Configure<ForwardedHeadersOptions>(options =>
        {
            options.ForwardedHeaders =
                ForwardedHeaders.XForwardedFor |
                ForwardedHeaders.XForwardedProto;
            options.KnownNetworks.Add(
                IPNetwork.Parse("0.0.0.0/0"));
        })

        // Register built-in/libraries services.
        .AddSerilog(Log.Logger)
        .AddCors()
        .AddHttpClient()
        .AddHttpContextAccessor()

        // Register custom services.
        .AddSingleton<IConfigurationService>(configurationService)
        .AddSingleton<DbConn>()
        .AddSingleton<IDbMigrationService, DbMigrationService>()
        .AddSingleton<ITimeService, TimeService>()
        .AddSingleton<IRandomService, RandomService>()
        .AddSingleton<IRulesRepository, RulesRepository>()
        .AddSingleton<IRuleService, RuleService>()
        .AddSingleton<IScheduleRepository, ScheduleRepository>()
        .AddSingleton<INyaaRssService, NyaaRssService>()
        .AddSingleton<ITransmissionService, TransmissionService>()
        .AddSingleton<IRunnerService, RunnerService>()
        
        // Background services.
        .AddHostedService<DbMigrationJob>()
        .AddHostedService<SchedulerJob>()
        
        // Middlewares.
        .AddTransient<ExceptionHandlerMiddleware>()
        .AddTransient<AuthenticationMiddleware>();
        
    WebApplication application = builder.Build();
    application
        // All starts with /api.
        .UsePathBase(configurationService.BasePath)

        // CORS.
        .UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .SetIsOriginAllowed(origin =>
                origin.Contains("localhost"))) // Allow from localhost.

        // Middlewares.
        .UseForwardedHeaders()
        .UseSerilogRequestLogging()
        .UseMiddleware<ExceptionHandlerMiddleware>()
        .UseMiddleware<AuthenticationMiddleware>()

        // Routing.
        .UseRouting();

    // Controllers.
    IndexController.Map(application);
    RulesController.Map(application);
    
    // Done.
    Log.Information(
        "Listening on: {0}",
        configurationService.ListenEndpoint);
    await application.RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Server error");
}
finally
{
    Log.CloseAndFlush();
}
