using System;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace torrent_getter.Services;

public interface IScheduleRepository
{
    Task<DateTime> GetLastRssItemTimeAsync();

    Task SetLastRssItemTimeAsync(
        DateTime time);
    
    Task<DateTime> GetLastDownloadTimeAsync(
        string ruleId);
    
    Task SetLastDownloadTimeAsync(
        string ruleId,
        DateTime time);
}

public class ScheduleRepository : IScheduleRepository
{
    DbConn m_conn;

    public ScheduleRepository(DbConn conn)
    {
        m_conn = conn;
    }
    
    public async Task<DateTime> GetLastRssItemTimeAsync()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT last_time
            FROM schedule_static
            LIMIT 1";
        object? result = await command.ExecuteScalarAsync();
        string? resultStr = result as string;

        if (string.IsNullOrEmpty(resultStr))
        {
            return default;
        }
        
        return DateTime.Parse(resultStr)
            .ToLocalTime()
            .ToUniversalTime();
    }

    public async Task SetLastRssItemTimeAsync(DateTime time)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteTransaction transaction = conn.BeginTransaction();

        try
        {
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                DELETE FROM schedule_static";
            await command.ExecuteNonQueryAsync();
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }

        try
        {
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO schedule_static (
                    last_time
                ) VALUES (
                    @lastTime
                )";
            command.Parameters.AddWithValue("@lastTime", time.ToString("yyyy-MM-dd HH:mm:ss"));
            await command.PrepareAsync();
            int count = await command.ExecuteNonQueryAsync();

            if (count != 1)
            {
                throw new Exception("Unable to save static schedule");
            }
            
            await transaction.CommitAsync();
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }
    }

    public async Task<DateTime> GetLastDownloadTimeAsync(string ruleId)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT last_time
            FROM schedules
            WHERE rule_id = @ruleId";
        command.Parameters.AddWithValue("@ruleId", ruleId);
        await command.PrepareAsync();
        using SqliteDataReader reader = await command.ExecuteReaderAsync();

        if (!await reader.ReadAsync())
        {
            return default;
        }

        return DateTime.Parse(reader.GetString(0))
            .ToLocalTime()
            .ToUniversalTime();
    }

    public async Task SetLastDownloadTimeAsync(string ruleId, DateTime time)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();

        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                DELETE FROM schedules
                WHERE rule_id = @ruleId";
            command.Parameters.AddWithValue("@ruleId", ruleId);
            await command.PrepareAsync();
            await command.ExecuteNonQueryAsync();
        }

        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                INSERT INTO schedules (
                    rule_id, last_time
                ) VALUES (
                    @ruleId, @lastTime
                )";
            command.Parameters.AddWithValue("@ruleId", ruleId);
            command.Parameters.AddWithValue(
                "@lastTime",
                time.ToString("yyyy-MM-dd HH:mm:ss"));
            await command.PrepareAsync();
            int count = await command.ExecuteNonQueryAsync();

            if (count != 1)
            {
                throw new Exception("Unable to save schedule");
            }
        }
    }
}
