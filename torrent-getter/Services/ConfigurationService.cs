using System;
using System.Security.Cryptography;

namespace torrent_getter.Services;

public interface IConfigurationService
{
    string BasePath
    {
        get;
    }

    string ListenEndpoint
    {
        get;
    }

    string DatabasePath
    {
        get;
    }

    int PollIntervalMins
    {
        get;
    }

    string AuthUser
    {
        get;
    }

    string AuthPassword
    {
        get;
    }

    string TransmissionApi
    {
        get;
    }

    string TransmissionApiUsername
    {
        get;
    }

    string TransmissionApiPassword
    {
        get;
    }
}

public class ConfigurationService : IConfigurationService
{
    public string BasePath => "/api";

    public string ListenEndpoint
    {
        get;
    }

    public string DatabasePath
    {
        get;
    }

    public int PollIntervalMins
    {
        get;
    }

    public string AuthUser
    {
        get;
    }

    public string AuthPassword
    {
        get;
    }

    public string TransmissionApi
    {
        get;
    }

    public string TransmissionApiUsername
    {
        get;
    }

    public string TransmissionApiPassword
    {
        get;
    }

    public ConfigurationService()
    {
        ListenEndpoint =
            Environment.GetEnvironmentVariable("LISTEN_ENDPOINT") ??
            throw new Exception("Missing LISTEN_ENDPOINT");
        DatabasePath =
            Environment.GetEnvironmentVariable("DATABASE_PATH") ??
            throw new Exception("Missing DATABASE_PATH");
        AuthUser =
            Environment.GetEnvironmentVariable("AUTH_USER") ??
            throw new Exception("Missing AUTH_USER");
        AuthPassword =
            Environment.GetEnvironmentVariable("AUTH_PASSWORD") ??
            throw new Exception("Missing AUTH_PASSWORD");
        TransmissionApi =
            Environment.GetEnvironmentVariable("TRANSMISSION_API") ??
            throw new Exception("Missing TRANSMISSION_API");
        TransmissionApiUsername =
            Environment.GetEnvironmentVariable("TRANSMISSION_API_USERNAME") ??
            throw new Exception("Missing TRANSMISSION_API_USERNAME");
        TransmissionApiPassword =
            Environment.GetEnvironmentVariable("TRANSMISSION_API_PASSWORD") ??
            throw new Exception("Missing TRANSMISSION_API_PASSWORD");
        
        string pollIntervalStr =
            Environment.GetEnvironmentVariable("POLL_INTERVAL_MINS") ??
            throw new Exception("Missing POLL_INTERVAL_MINS");
        
        if (!int.TryParse(pollIntervalStr, out int pollIntervalMins))
        {
            throw new Exception("Invalid POLL_INTERVAL_MINS");
        }

        PollIntervalMins = pollIntervalMins;
    }
}
