using System;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace torrent_getter.Services;

public abstract record AddRuleResult();

public record DuplicateAddRuleResult() :
    AddRuleResult;

public record SuccessAddRuleResult(Rule Rule) :
    AddRuleResult;

public abstract record UpdateRuleResult();

public record NotFoundUpdateRuleResult() :
    UpdateRuleResult;

public record DuplicateUpdateRuleResult() :
    UpdateRuleResult;

public record SuccessUpdateRuleResult(Rule Rule) :
    UpdateRuleResult;

public interface IRuleService
{
    /// <summary>
    /// Adds a new watch rule.
    /// This will check for duplicates on every arguments.
    /// </summary>
    Task<AddRuleResult> AddAsync(
        string name,
        string directory,
        string[][] contains);

    /// <summary>
    /// Updates a rule.
    /// This will check for duplicates on every arguments.
    /// </summary>
    Task<UpdateRuleResult> UpdateAsync(
        string id,
        string name,
        string directory,
        string[][] contains);

    /// <summary>
    /// Deletes a watch rule.
    /// </summary>
    Task DeleteAsync(string id);
}

public class RuleService : IRuleService
{
    IRulesRepository m_rulesRepository;
    IRandomService m_randomService;

    ILogger Log { get; } = Serilog.Log.Logger.ForContext<RuleService>();

    public RuleService(
        IRulesRepository rulesRepository,
        IRandomService randomService)
    {
        m_rulesRepository = rulesRepository;
        m_randomService = randomService;
    }
    
    public async Task<AddRuleResult> AddAsync(string name, string directory, string[][] contains)
    {
        RuleItem[] items = contains
            .Select(x => new RuleItem(x))
            .ToArray();
        Rule rule = new Rule(
            m_randomService.GenerateId(),
            name,
            directory,
            items);

        ILogger log = Log
            .ForContext("name", name)
            .ForContext("directory", directory);
        log.Information("Validating new rule");
        Rule[] rules = await m_rulesRepository.GetAsync();
        Rule? duplicate = rules
            .Where(x => x.Name.ToLower() == name.ToLower())
            .FirstOrDefault();

        if (duplicate != null)
        {
            return new DuplicateAddRuleResult();
        }

        foreach (Rule currentRule in rules)
        {
            if (currentRule.Name == name ||
                currentRule.Directory == directory)
            {
                return new DuplicateAddRuleResult();
            }

            foreach (RuleItem currentRuleItem in currentRule.Items)
            {
                foreach (string[] newRuleItemContains in contains)
                {
                    if (newRuleItemContains.Order().SequenceEqual(
                        currentRuleItem.Contains.Order()))
                    {
                        return new DuplicateAddRuleResult();
                    }
                }
            }
        }

        log.Information("Saving new rule");
        rules = rules
            .Append(rule)
            .ToArray();
        await m_rulesRepository.SaveAsync(rules);

        return new SuccessAddRuleResult(rule);
    }

    public async Task<UpdateRuleResult> UpdateAsync(string id, string name, string directory, string[][] contains)
    {
        ILogger log = Log
            .ForContext("id", id)
            .ForContext("name", name)
            .ForContext("directory", directory);
        log.Information("Validating rule id");
        Rule[] rules = await m_rulesRepository.GetAsync();
        Rule? rule = rules
            .Where(x => x.Id == id)
            .FirstOrDefault();

        if (rule == null)
        {
            return new NotFoundUpdateRuleResult();
        }

        log.Information("Validating rule update");
        var existingRules = rules.Where(x => x.Id != id);
        Rule? duplicate = existingRules
            .Where(x => x.Name.ToLower() == name.ToLower())
            .FirstOrDefault();

        if (duplicate != null)
        {
            return new DuplicateUpdateRuleResult();
        }

        foreach (Rule currentRule in existingRules)
        {
            if (currentRule.Name == name ||
                currentRule.Directory == directory)
            {
                return new DuplicateUpdateRuleResult();
            }

            foreach (RuleItem currentRuleItem in currentRule.Items)
            {
                foreach (string[] newRuleItemContains in contains)
                {
                    if (newRuleItemContains.Order().SequenceEqual(
                        currentRuleItem.Contains.Order()))
                    {
                        return new DuplicateUpdateRuleResult();
                    }
                }
            }
        }

        log.Information("Saving rule update");
        rule = rule with
        {
            Name = name,
            Directory = directory,
            Items = contains
                .Select(x => new RuleItem(x))
                .ToArray()
        };
        rules = rules
            .Select(x => x.Id == id ? rule : x)
            .ToArray();
        await m_rulesRepository.SaveAsync(rules);

        return new SuccessUpdateRuleResult(rule);
    }

    public async Task DeleteAsync(string id)
    {
        ILogger log = Log
            .ForContext("id", id);
        log.Information("Validating rule id");
        Rule[] rules = await m_rulesRepository.GetAsync();
        Rule? rule = rules
            .Where(x => x.Id == id)
            .FirstOrDefault();

        if (rule == null)
        {
            return;
        }

        log.Information("Removing rule");
        rules = rules
            .Where(x => x.Id != id)
            .ToArray();
        await m_rulesRepository.SaveAsync(rules);
    }
}
