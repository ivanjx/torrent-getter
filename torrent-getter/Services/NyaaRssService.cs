using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace torrent_getter.Services;

public record RssItem(
    string Id,
    string Name,
    string Link,
    DateTime Time);

public interface INyaaRssService
{
    /// <summary>
    /// Fetches rss items from nyaa.
    /// </summary>
    Task<RssItem[]> GetItemsAsync(CancellationToken cancellationToken);
}

public partial class NyaaRssService : INyaaRssService
{
    const string RSS_URL = "https://nyaa.si/rss";

    IHttpClientFactory m_clientFactory;

    public NyaaRssService(IHttpClientFactory clientFactory)
    {
        m_clientFactory = clientFactory;
    }

    [GeneratedRegex(@"\d+")]
    private partial Regex NumberRegex();
    
    public async Task<RssItem[]> GetItemsAsync(CancellationToken cancellationToken)
    {
        using HttpClient client = m_clientFactory.CreateClient();
        string raw = await client.GetStringAsync(
            RSS_URL,
            cancellationToken: cancellationToken);
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(raw);
        XmlNodeList? itemNodes = doc.SelectNodes(
            "/rss/channel/item");

        if (itemNodes == null)
        {
            throw new Exception("Invalid rss xml: " +  raw);
        }

        List<RssItem> result = new List<RssItem>();

        foreach (XmlNode itemNode in itemNodes)
        {
            XmlNode? titleNode = itemNode.SelectSingleNode("./title");
            XmlNode? linkNode = itemNode.SelectSingleNode("./link");
            XmlNode? timeNode = itemNode.SelectSingleNode("./pubDate");

            if (titleNode == null ||
                linkNode == null ||
                timeNode == null)
            {
                throw new Exception("Invalid rss xml: " + raw);
            }

            string title = titleNode.InnerText;
            string link = linkNode.InnerText;
            string id = NumberRegex().Match(link).Value;
            DateTime time = DateTime.Parse(timeNode.InnerText).ToUniversalTime();

            result.Insert(
                0,
                new RssItem(
                    id,
                    title,
                    link,
                    time));
        }

        return result.ToArray();
    }
}
