using System;

namespace torrent_getter.Services;

public interface IRandomService
{
    /// <summary>
    /// Generates new guid string.
    /// </summary>
    string GenerateId();
    
    /// <summary>
    /// Generates random positive integer.
    /// </summary>
    int GenerateInt();
}

public class RandomService : IRandomService
{
    public string GenerateId()
    {
        return Guid.NewGuid().ToString();
    }

    public int GenerateInt()
    {
        return Random.Shared.Next(0, int.MaxValue);
    }
}
