using System.IO;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace torrent_getter.Services;

public class DbConn
{
    IConfigurationService m_configurationService;

    public DbConn(IConfigurationService configurationService)
    {
        m_configurationService = configurationService;
    }
    
    public async Task<SqliteConnection> CreateAsync()
    {
        string? dbDir = Path.GetDirectoryName(m_configurationService.DatabasePath);
        
        if (!string.IsNullOrEmpty(dbDir))
        {
            Directory.CreateDirectory(dbDir);
        }
        
        SqliteConnection conn = new SqliteConnection(
            string.Format(
                "Data Source={0};",
                m_configurationService.DatabasePath));
        
        try
        {
            await conn.OpenAsync();
            return conn;
        }
        catch
        {
            conn.Dispose();
            throw;
        }
    }

    public async Task InitAsync()
    {
        using SqliteConnection conn = await CreateAsync();
        using SqliteTransaction transaction = conn.BeginTransaction();
        using SqliteCommand command = conn.CreateCommand();
        command.Transaction = transaction;
        command.CommandText = @"
            CREATE TABLE IF NOT EXISTS `version` (
                `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                `version`   INTEGER NOT NULL
            );

            CREATE TABLE IF NOT EXISTS `rules` (
                `num`       INTEGER PRIMARY KEY AUTOINCREMENT,
                `id`        TEXT NOT NULL UNIQUE,
                `name`      TEXT NOT NULL UNIQUE,
                `directory` TEXT NOT NULL
            );

            CREATE TABLE IF NOT EXISTS `rule_items` (
                `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                `rule_id`   TEXT NOT NULL,
                `item_idx`  INTEGER NOT NULL,
                `contain`   TEXT NOT NULL,
                FOREIGN KEY (`rule_id`) REFERENCES `rules`(`id`)
            );

            CREATE TABLE IF NOT EXISTS `schedules` (
                `rule_id`   TEXT PRIMARY KEY,
                `last_time` TEXT NOT NULL
            );
            
            CREATE TABLE IF NOT EXISTS `schedule_static` (
                `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                `last_time` TEXT NOT NULL
            );";
        await command.ExecuteNonQueryAsync();
        await transaction.CommitAsync();
    }
}
