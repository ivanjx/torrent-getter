using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace torrent_getter.Services;

public interface IRulesRepository
{
    Task<Rule[]> GetAsync();

    Task SaveAsync(Rule[] rules);
}

public class RulesRepository : IRulesRepository
{
    DbConn m_conn;
    
    public RulesRepository(DbConn conn)
    {
        m_conn = conn;
    }

    public async Task<Rule[]> GetAsync()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteCommand command = conn.CreateCommand();
        command.CommandText = @"
            SELECT
                r.id, r.name, r.directory,
                ri.item_idx, ri.contain
            FROM rules AS r
            INNER JOIN rule_items AS ri
                ON ri.rule_id = r.id";
        using SqliteDataReader reader = await command.ExecuteReaderAsync();
        Dictionary<string, Rule> result = new Dictionary<string, Rule>();
        Dictionary<string, List<List<string>>> contains = new Dictionary<string, List<List<string>>>();

        while (await reader.ReadAsync())
        {
            string id = reader.GetString(0);
            string name = reader.GetString(1);
            string directory = reader.GetString(2);

            if (!result.ContainsKey(id))
            {
                result.Add(
                    id,
                    new Rule(
                        id,
                        name,
                        directory,
                        Array.Empty<RuleItem>()));
            }

            int containIdx = reader.GetInt32(3);
            string contain = reader.GetString(4);

            if (!contains.ContainsKey(id))
            {
                contains.Add(id, new List<List<string>>());
            }

            if (contains[id].Count < containIdx + 1)
            {
                contains[id].Add(
                    new List<string>());
            }

            contains[id][containIdx].Add(contain);
        }

        return result.Values
            .Select(x =>
            {
                return x with
                {
                    Items = contains[x.Id]
                        .Select(xx => new RuleItem(xx.ToArray()))
                        .ToArray()
                };
            })
            .ToArray();
    }

    public async Task SaveAsync(Rule[] rules)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteTransaction transaction = conn.BeginTransaction();

        try
        {
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                DELETE FROM rule_items;
                DELETE FROM rules;";
            await command.ExecuteNonQueryAsync();
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }

        try
        {
            foreach (Rule rule in rules)
            {
                using SqliteCommand command = conn.CreateCommand();
                command.Transaction = transaction;
                command.CommandText = @"
                    INSERT INTO rules (
                        id, name, directory
                    ) VALUES (
                        @id, @name, @directory
                    )";
                command.Parameters.AddWithValue("@id", rule.Id);
                command.Parameters.AddWithValue("@name", rule.Name);
                command.Parameters.AddWithValue("@directory", rule.Directory);
                await command.PrepareAsync();
                int count = await command.ExecuteNonQueryAsync();

                if (count != 1)
                {
                    throw new Exception("Unable to save rules");
                }
            }

            foreach (Rule rule in rules)
            {
                int index = 0;
                
                foreach (RuleItem ruleItem in rule.Items)
                {
                    foreach (string contain in ruleItem.Contains)
                    {
                        using SqliteCommand command = conn.CreateCommand();
                        command.Transaction = transaction;
                        command.CommandText = @"
                        INSERT INTO rule_items (
                            rule_id, item_idx, contain
                        ) VALUES (
                            @id, @index, @contain
                        )";
                        command.Parameters.AddWithValue("@id", rule.Id);
                        command.Parameters.AddWithValue("@index", index);
                        command.Parameters.AddWithValue("@contain", contain);
                        await command.PrepareAsync();
                        int count = await command.ExecuteNonQueryAsync();

                        if (count != 1)
                        {
                            throw new Exception("Unable to save rule items");
                        }
                    }
                    
                    ++index;
                }
            }

            await transaction.CommitAsync();
        }
        catch
        {
            await transaction.RollbackAsync();
            throw;
        }
    }
}
