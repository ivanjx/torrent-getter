using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace torrent_getter.Services;

public interface IRunnerService
{
    /// <summary>
    /// Gets latest rss list and downloads all the entries
    /// matching the rules. All previously processed rules
    /// are ignored. This will be run periodically.
    /// </summary>
    Task RunAsync(CancellationToken cancellationToken);
}

public class RunnerService : IRunnerService
{
    IRulesRepository m_rulesRepository;
    IScheduleRepository m_scheduleRepository;
    INyaaRssService m_nyaaRssService;
    ITransmissionService m_transmissionService;

    ILogger Log { get; } = Serilog.Log.Logger.ForContext<RunnerService>();

    public RunnerService(
        IRulesRepository rulesRepository,
        IScheduleRepository scheduleRepository,
        INyaaRssService nyaaRssService,
        ITransmissionService transmissionService)
    {
        m_rulesRepository = rulesRepository;
        m_scheduleRepository = scheduleRepository;
        m_nyaaRssService = nyaaRssService;
        m_transmissionService = transmissionService;
    }

    bool Match(string str, string[] rules)
    {
        foreach (string rule in rules)
        {
            if (!str.Contains(rule))
            {
                return false;
            }
        }

        return true;
    }
    
    public async Task RunAsync(CancellationToken cancellationToken)
    {
        Log.Information("Retrieving repositories");
        DateTime lastItemTime = await m_scheduleRepository.GetLastRssItemTimeAsync();
        Rule[] rules = await m_rulesRepository.GetAsync();
        
        Log.Information("Retrieving nyaa rss");
        RssItem[] rssItems = await m_nyaaRssService.GetItemsAsync(cancellationToken);
        rssItems = rssItems
            .Where(x => x.Time > lastItemTime)
            .ToArray();

        if (rssItems.Length == 0)
        {
            Log.Information("No new nyaa rss items");
            return;
        }

        foreach (RssItem rssItem in rssItems)
        {
            // Checking for item name.
            Rule? matchingRule = rules
                .Where(x =>
                {
                    foreach (RuleItem ruleItem in x.Items)
                    {
                        if (Match(rssItem.Name, ruleItem.Contains))
                        {
                            return true;
                        }
                    }

                    return false;
                })
                .FirstOrDefault();

            if (matchingRule == null)
            {
                continue;
            }
            
            // Checking last download time.
            DateTime lastRuleDownloadTime = await m_scheduleRepository.GetLastDownloadTimeAsync(
                matchingRule.Id);
            TimeSpan lastDownloadAge = rssItem.Time - lastRuleDownloadTime;

            if (lastDownloadAge < TimeSpan.FromHours(1))
            {
                // Check again after 1 hour.
                continue;
            }

            Log
                .ForContext("item", rssItem)
                .ForContext("rule", matchingRule)
                .Information("Downloading torrent");
            await m_transmissionService.DownloadAsync(
                rssItem.Link,
                matchingRule.Directory,
                cancellationToken);
            
            // Updating last download time.
            await m_scheduleRepository.SetLastDownloadTimeAsync(
                matchingRule.Id,
                rssItem.Time);
        }
        
        // Updating last rss item time.
        await m_scheduleRepository.SetLastRssItemTimeAsync(
            rssItems.Last().Time);
        Log
            .ForContext("count", rssItems.Length)
            .ForContext("lastRss", rssItems.Last())
            .Information("Done scanning nyaa rss");
    }
}
