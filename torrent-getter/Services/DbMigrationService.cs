using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Serilog;

namespace torrent_getter.Services;

public interface IDbMigrationService
{
    Task MigrateAsync(CancellationToken cancellationToken);
}

public class DbMigrationService : IDbMigrationService
{
    DbConn m_conn;

    ILogger Log { get; } = Serilog.Log.ForContext<DbMigrationService>();

    public DbMigrationService(DbConn conn)
    {
        m_conn = conn;
    }

    async Task<long> GetVersionAsync()
    {
        using SqliteConnection conn = await m_conn.CreateAsync();

        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                SELECT COUNT(*)
                FROM sqlite_master
                WHERE
                    type = @type AND
                    name = @name";
            command.Parameters.AddWithValue("@type", "table");
            command.Parameters.AddWithValue("@name", "version");
            await command.PrepareAsync();
            object? result = await command.ExecuteScalarAsync();

            if (result is null ||
                result is not long available ||
                available == 0)
            {
                return 0;
            }
        }

        {
            using SqliteCommand command = conn.CreateCommand();
            command.CommandText = @"
                SELECT version
                FROM version";
            object? result = await command.ExecuteScalarAsync();

            if (result is null ||
                result is not long version)
            {
                return 0;
            }

            return version;
        }
    }

    async Task MigrateToV2Async(CancellationToken cancellationToken)
    {
        using SqliteConnection conn = await m_conn.CreateAsync();
        using SqliteTransaction transaction = conn.BeginTransaction();

        try
        {
            Log.Information("Deleting unnecessary columns on table rules");
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                ALTER TABLE `rules` DROP COLUMN `watchday`;
                ALTER TABLE `rules` DROP COLUMN `watchtime`;
                ALTER TABLE `rules` DROP COLUMN `watchmin`;";
            await command.ExecuteNonQueryAsync(cancellationToken);
        }
        catch
        {
            Log.Information("Rolling back transaction");
            await transaction.RollbackAsync();
            throw;
        }

        try
        {
            Log.Information("Creating table version");
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                CREATE TABLE IF NOT EXISTS `version` (
                    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
                    `version`   INTEGER NOT NULL
                )";
            await command.ExecuteNonQueryAsync(cancellationToken);
        }
        catch
        {
            Log.Information("Rolling back transaction");
            await transaction.RollbackAsync();
            throw;
        }

        try
        {
            Log.Information("Setting table version to V2");
            using SqliteCommand command = conn.CreateCommand();
            command.Transaction = transaction;
            command.CommandText = @"
                INSERT INTO version (version) VALUES (@version)";
            command.Parameters.AddWithValue("@version", 2);
            await command.PrepareAsync();
            await command.ExecuteNonQueryAsync(cancellationToken);
        }
        catch
        {
            Log.Information("Rolling back transaction");
            await transaction.RollbackAsync();
            throw;
        }

        try
        {
            Log.Information("Committing changes");
            await transaction.CommitAsync(cancellationToken);
        }
        catch
        {
            Log.Information("Rolling back transaction");
            await transaction.RollbackAsync();
            throw;
        }
    }

    public async Task MigrateAsync(CancellationToken cancellationToken)
    {
        long version = await GetVersionAsync();

        if (version < 2)
        {
            Log.Information("Migrating database to V2");
            await MigrateToV2Async(cancellationToken);
        }
    }
}
