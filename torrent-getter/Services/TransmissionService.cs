using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace torrent_getter.Services;

public interface ITransmissionService
{
    /// <summary>
    /// Sends download request to a transmission endpoint API.
    /// </summary>
    Task DownloadAsync(
        string torrentUrl,
        string downloadDir,
        CancellationToken cancellationToken);
}

public class TransmissionService : ITransmissionService
{
    IConfigurationService m_configurationService;
    IRandomService m_randomService;
    IHttpClientFactory m_clientFactory;

    public TransmissionService(
        IConfigurationService configurationService,
        IRandomService randomService,
        IHttpClientFactory clientFactory)
    {
        m_configurationService = configurationService;
        m_randomService = randomService;
        m_clientFactory = clientFactory;
    }
    
    public async Task DownloadAsync(string torrentUrl, string downloadDir, CancellationToken cancellationToken)
    {
        using HttpClient client = m_clientFactory.CreateClient();
        
        string authStr = string.Format(
            "{0}:{1}",
            m_configurationService.TransmissionApiUsername,
            m_configurationService.TransmissionApiPassword);
        TorrentAddArgumentsRequest arguments = new TorrentAddArgumentsRequest(
            torrentUrl,
            downloadDir);
        int tag = m_randomService.GenerateInt();
        TorrentAddRequest request = new TorrentAddRequest(
            tag,
            arguments);

        string responseRaw;
        bool success;
        string? sessionId = null;
        
        {
            using HttpRequestMessage requestMessage = new HttpRequestMessage();
            requestMessage.RequestUri = new Uri(m_configurationService.TransmissionApi);
            requestMessage.Method = HttpMethod.Post;
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes(authStr)));
            requestMessage.Content = JsonContent.Create(
                request,
                TransmissionServiceContext.Default.TorrentAddRequest);

            using HttpResponseMessage responseMessage = await client.SendAsync(
                requestMessage,
                cancellationToken);
            responseRaw = await responseMessage.Content.ReadAsStringAsync(
                cancellationToken);
            success = responseMessage.IsSuccessStatusCode;
            if (responseMessage.Headers.TryGetValues(
                    "X-Transmission-Session-Id",
                    out IEnumerable<string>? sessionIds))
            {
                sessionId = sessionIds.FirstOrDefault();
            }
        }

        if (!success &&
            !string.IsNullOrEmpty(sessionId))
        {
            using HttpRequestMessage requestMessage = new HttpRequestMessage();
            requestMessage.RequestUri = new Uri(m_configurationService.TransmissionApi);
            requestMessage.Method = HttpMethod.Post;
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes(authStr)));
            requestMessage.Headers.Add(
                "X-Transmission-Session-Id",
                sessionId);
            requestMessage.Content = JsonContent.Create(
                request,
                TransmissionServiceContext.Default.TorrentAddRequest);
            using HttpResponseMessage responseMessage = await client.SendAsync(
                requestMessage,
                cancellationToken);
            responseRaw = await responseMessage.Content.ReadAsStringAsync(
                cancellationToken);
            success = responseMessage.IsSuccessStatusCode;
        }

        if (!success)
        {
            throw new Exception("Error sending add torrent command: " + responseRaw);
        }

        JsonDocument response = JsonDocument.Parse(responseRaw);
        string? responseResult = response.RootElement.GetProperty("result").GetString();
        int responseTag = response.RootElement.GetProperty("tag").GetInt32();
        bool added = response.RootElement.GetProperty("arguments").TryGetProperty("torrent-added", out _);

        if (responseResult != "success")
        {
            throw new Exception("Invalid response result: " + responseResult);
        }

        if (responseTag != tag)
        {
            throw new Exception("Invalid response tag: " + responseTag);
        }

        if (!added)
        {
            throw new Exception("Invalid response: " + responseRaw);
        }
    }

    public record TorrentAddArgumentsRequest
    {
        [JsonPropertyName("start")]
        public bool Start
        {
            get;
            set;
        }

        [JsonPropertyName("bandwidthPriority")]
        public int BandwidthPriority
        {
            get;
            set;
        }

        [JsonPropertyName("downloadDir")]
        public string DownloadDir1
        {
            get;
            set;
        }

        [JsonPropertyName("download-dir")]
        public string DownloadDir2
        {
            get;
            set;
        }

        [JsonPropertyName("paused")]
        public bool Paused
        {
            get;
            set;
        }

        [JsonPropertyName("filename")]
        public string FileName
        {
            get;
            set;
        }

        public TorrentAddArgumentsRequest(
            string fileName,
            string downloadDir)
        {
            FileName = fileName;
            DownloadDir1 = downloadDir;
            DownloadDir2 = downloadDir;
            Start = true;
        }
    }

    public record TorrentAddRequest
    {
        [JsonPropertyName("method")]
        public string Method
        {
            get;
            set;
        }

        [JsonPropertyName("tag")]
        public int Tag
        {
            get;
            set;
        }

        [JsonPropertyName("arguments")]
        public TorrentAddArgumentsRequest Arguments
        {
            get;
            set;
        }

        public TorrentAddRequest(
            int tag,
            TorrentAddArgumentsRequest arguments)
        {
            Method = "torrent-add";
            Tag = tag;
            Arguments = arguments;
        }
    }
}

[JsonSerializable(typeof(TransmissionService.TorrentAddRequest))]
public partial class TransmissionServiceContext : JsonSerializerContext
{
}
