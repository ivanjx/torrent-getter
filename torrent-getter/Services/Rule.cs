using System;

namespace torrent_getter.Services;

public record RuleItem(
    string[] Contains);

public record Rule(
    string Id,
    string Name,
    string Directory,
    RuleItem[] Items);
