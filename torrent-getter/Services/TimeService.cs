using System;
using System.Threading.Tasks;

namespace torrent_getter.Services;

public interface ITimeService
{
    DateTime Now
    {
        get;
    }
}

public class TimeService : ITimeService
{
    public DateTime Now
    {
        get => DateTime.UtcNow;
    }
}
