using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Serilog;
using torrent_getter.Services;
using Exception = System.Exception;

namespace torrent_getter.Jobs;

public class SchedulerJob(
    IConfigurationService _configurationService,
    IRunnerService _runnerService) :
    BackgroundService
{
    ILogger Log { get; } = Serilog.Log.ForContext<SchedulerJob>();
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Log.Information("Scheduler job started");
        
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await _runnerService.RunAsync(stoppingToken);
                await Task.Delay(
                    TimeSpan.FromMinutes(_configurationService.PollIntervalMins),
                    stoppingToken);
            }
            catch (OperationCanceledException)
            {
                break;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Scheduler job error");
                await Task.Delay(
                    TimeSpan.FromSeconds(1),
                    CancellationToken.None);
            }
        }

        Log.Information("Scheduler job stopped");
    }
}
