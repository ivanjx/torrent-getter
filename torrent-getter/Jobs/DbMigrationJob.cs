using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Serilog;
using torrent_getter.Services;

namespace torrent_getter.Jobs;

public class DbMigrationJob(
    DbConn _dbConn,
    IDbMigrationService _migrationService) :
    BackgroundService
{
    ILogger Log { get; } = Serilog.Log.ForContext<DbMigrationJob>();

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Log.Information("DB migration job started");
        
        try
        {
            await _dbConn.InitAsync();
            await _migrationService.MigrateAsync(stoppingToken);
            Log.Information("DB migration job done");
        }
        catch (Exception ex)
        {
            Log.Error(ex, "DB migration job error");
        }
    }
}
