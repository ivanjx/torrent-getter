using System;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using torrent_getter.Services;

namespace torrent_getter.Controllers;

public class RulesController
{
    public static void Map(WebApplication app)
    {
        RulesController controller = new RulesController(
            app.Services.GetRequiredService<IRulesRepository>(),
            app.Services.GetRequiredService<IRuleService>());
        app.MapGet("/rules", controller.GetAsync);
        app.MapPost("/rules/add", controller.AddAsync);
        app.MapPost("/rules/update", controller.UpdateAsync);
        app.MapPost("/rules/delete", controller.DeleteAsync);
    }

    IRulesRepository m_rulesRepository;
    IRuleService m_ruleService;

    public RulesController(
        IRulesRepository rulesRepository,
        IRuleService ruleService)
    {
        m_rulesRepository = rulesRepository;
        m_ruleService = ruleService;
    }

    public record RuleItemsResponse
    {
        [JsonPropertyName("contains")]
        public string[] Contains
        {
            get;
            set;
        }

        public RuleItemsResponse(string[] contains)
        {
            Contains = contains;
        }
    }

    public record RuleResponse
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonPropertyName("directory")]
        public string Directory
        {
            get;
            set;
        }

        [JsonPropertyName("items")]
        public RuleItemsResponse[] Items
        {
            get;
            set;
        }

        public RuleResponse(Rule rule)
        {
            Id = rule.Id;
            Name = rule.Name;
            Directory = rule.Directory;
            Items = rule.Items
                .Select(x => new RuleItemsResponse(x.Contains))
                .ToArray();
        }
    }
    
    [Authenticate]
    public async Task<RuleResponse[]> GetAsync()
    {
        Rule[] rules = await m_rulesRepository.GetAsync();
        return rules
            .Select(x => new RuleResponse(x))
            .ToArray();
    }

    public record RuleItemsRequest
    {
        [JsonPropertyName("contains")]
        public string[]? Contains
        {
            get;
            set;
        }
    }

    public record RuleRequest
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string? Name
        {
            get;
            set;
        }

        [JsonPropertyName("directory")]
        public string? Directory
        {
            get;
            set;
        }

        [JsonPropertyName("items")]
        public RuleItemsRequest[]? Items
        {
            get;
            set;
        }
    }
    
    [Authenticate]
    public async Task<RuleRequest> AddAsync(RuleRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Name) ||
            string.IsNullOrEmpty(request.Directory) ||
            request.Items == null ||
            request.Items.Length == 0)
        {
            throw new ApiException("missing_parameter");
        }

        string[][] contains = request.Items
            .Select(x =>
            {
                if (x.Contains == null)
                {
                    throw new ApiException("missing_parameters");
                }

                return x.Contains;
            })
            .ToArray();

        AddRuleResult result = await m_ruleService.AddAsync(
            request.Name,
            request.Directory,
            contains);

        if (result is DuplicateAddRuleResult)
        {
            throw new ApiException("duplicate_rule");
        }
        else if (result is SuccessAddRuleResult success)
        {
            return new RuleRequest
            {
                Id = success.Rule.Id,
                Name = success.Rule.Name,
                Directory = success.Rule.Directory,
                Items = success.Rule.Items
                    .Select(x =>
                        new RuleItemsRequest
                        {
                            Contains = x.Contains
                        })
                    .ToArray()
            };
        }
        else
        {
            throw new Exception("Unhandled add rule result");
        }
    }

    [Authenticate]
    public async Task<RuleRequest> UpdateAsync(RuleRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id) ||
            string.IsNullOrEmpty(request.Name) ||
            string.IsNullOrEmpty(request.Directory) ||
            request.Items == null ||
            request.Items.Length == 0)
        {
            throw new ApiException("missing_parameter");
        }

        string[][] contains = request.Items
            .Select(x =>
            {
                if (x.Contains == null)
                {
                    throw new ApiException("missing_parameters");
                }

                return x.Contains;
            })
            .ToArray();

        UpdateRuleResult result = await m_ruleService.UpdateAsync(
            request.Id,
            request.Name,
            request.Directory,
            contains);

        if (result is NotFoundUpdateRuleResult)
        {
            throw new ApiException("not_found");
        }
        else if (result is DuplicateUpdateRuleResult)
        {
            throw new ApiException("duplicate_rule");
        }
        else if (result is SuccessUpdateRuleResult success)
        {
            return new RuleRequest
            {
                Id = success.Rule.Id,
                Name = success.Rule.Name,
                Directory = success.Rule.Directory,
                Items = success.Rule.Items
                    .Select(x =>
                        new RuleItemsRequest
                        {
                            Contains = x.Contains
                        })
                    .ToArray()
            };
        }
        else
        {
            throw new Exception("Unhandled update rule result");
        }
    }
    
    [Authenticate]
    public async Task DeleteAsync(RuleRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id))
        {
            throw new ApiException("missing_parameters");
        }

        await m_ruleService.DeleteAsync(request.Id);
    }
}
