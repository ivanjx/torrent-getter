using System;

namespace torrent_getter.Controllers;

public class ApiException : Exception
{
    public string Name
    {
        get;
    }

    public ApiException(string name) :
        base(name)
    {
        Name = name;
    }
}
