using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Text.Json.Serialization;

namespace torrent_getter.Controllers;

public class ExceptionHandlerMiddleware : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next.Invoke(context);
        }
        catch (ApiException ex)
        {
            Log.Error(ex, ex.Message);

            // Override response.
            if (ex.Name == "not_authenticated" ||
                ex.Name == "invalid_auth")
            {
                context.Response.Headers.Append(
                    "WWW-Authenticate",
                    "Basic realm=\"Login to Torrent Getter\"");
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            }
            else if (ex.Name == "not_found")
            {
                context.Response.StatusCode = StatusCodes.Status404NotFound;
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    ex.Name,
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
        catch (Exception ex)
        {
            Log.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult(
                    "internal_server_error",
                    context.Connection.Id),
                ControllerJsonContext.Default.ErrorResult);
        }
    }

    public record ErrorResult
    {
        [JsonPropertyName("error")]
        public string Error
        {
            get;
            set;
        }

        [JsonPropertyName("traceId")]
        public string TraceId
        {
            get;
            set;
        }

        public ErrorResult(string error, string traceId)
        {
            Error = error;
            TraceId = traceId;
        }
    }
}
