using System;

namespace torrent_getter.Controllers;

[AttributeUsage(AttributeTargets.Method)]
public class AuthenticateAttribute : Attribute
{
}
