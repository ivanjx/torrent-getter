using System;
using System.Text.Json.Serialization;

namespace torrent_getter.Controllers;

[JsonSerializable(typeof(ExceptionHandlerMiddleware.ErrorResult))]
[JsonSerializable(typeof(string))]
[JsonSerializable(typeof(RulesController.RuleResponse[]))]
[JsonSerializable(typeof(RulesController.RuleRequest))]
public partial class ControllerJsonContext : JsonSerializerContext
{
}
