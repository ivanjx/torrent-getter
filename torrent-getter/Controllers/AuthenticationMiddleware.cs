using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using torrent_getter.Services;

namespace torrent_getter.Controllers;

public class AuthenticationMiddleware : IMiddleware
{
    IConfigurationService m_configurationService;

    public AuthenticationMiddleware(
        IConfigurationService configurationService)
    {
        m_configurationService = configurationService;
    }
    
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        AuthenticateAttribute? attribute = context.Features
            .Get<IEndpointFeature>()?
            .Endpoint?
            .Metadata
            .GetMetadata<AuthenticateAttribute>();

        if (attribute != null)
        {
            string? authHeader = context.Request.Headers["Authorization"];

            if (authHeader == null)
            {
                throw new ApiException("not_authenticated");
            }
            
            string credential = authHeader.Substring("Basic ".Length).Trim();
            byte[] credentialBuff = Convert.FromBase64String(credential);
            string[] credentials = Encoding.ASCII.GetString(credentialBuff).Split(':');

            if (credentials.Length < 2 ||
                credentials[0] != m_configurationService.AuthUser ||
                credentials[1] != m_configurationService.AuthPassword)
            {
                throw new ApiException("invalid_auth");
            }
        }
        
        await next(context);
    }
}
